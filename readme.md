This is a report on the fundamentals of listed companies in the water industry, part of the public utility, in China.
This project is maintained by Shihui Guo

The project uses restructured text as input, and Sphinx to generate the HTML files out of the restructured text.

This project also uses the plugin Exceltable to import excel documents as tables in the text.

To install pip
```
sudo easy_install pip
```

To install Sphinx
```
sudo pip install Sphinx
```

To generate the files, please run:
```
make html
```
The HTML files should be generated under the _build folder

To install exceltable
```
sudo pip install sphinxcontrib-exceltable
```
